<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 30.07.17
 * Time: 1:06
 */

namespace App\Facebook\Tests;

use App\Entities\Page;
use App\Entities\Post;
use App\Facebook\PageReader;
use App\TestCase;
use App\Tests\Output;
use Facebook\Facebook;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use PHPUnit\Framework\Exception;

/**
 * Class PageReaderTest
 *
 * @group facebook
 */
class PageReaderTest extends TestCase
{
    public function testGetPosts()
    {
        $page = new Page();
        $page->setId(111);

        $pageReader = $this->createPageReader();

        $posts = $pageReader->getPosts($page);

        $testPostData = [
            'id'                   => '286483085526_10158603864655527',
            'message'              => 'Teď už můžete bez obav datovat v EU za stejné ceny jako doma. Sbalte si data na dovolenou! Více na t-mobile.cz/tarify.',
            'created_at_timestamp' => '1494955979',
        ];

        /** @var Post $testPost */
        $testPost = $posts->get($testPostData['id']);

        $this->assertEquals(25, $posts->count());
        $this->assertEquals($testPostData['id'], $testPost->getId());
        $this->assertEquals($testPostData['message'], $testPost->getMessage());
        $this->assertEquals($testPostData['created_at_timestamp'], $testPost->getCreatedAt()->getTimestamp());
    }

    public function testGetPostsOverLimit()
    {
        $page2 = new Page();
        $page2->setId(222);

        $pageReader = $this->createPageReader();

        $posts = $pageReader->getPosts($page2, 200);

        $testPostData = [
            'id'                   => '120461304659141_1298931403478786',
            'message'              => 'Tak jo, už na nás jde vánoční nálada :) Co u vás?',
            'created_at_timestamp' => '1482502543',
            'story'                => 'O2 CZ shared Johny Machette\'s video.',
        ];

        /** @var Post $testPost */
        $testPost = $posts->get($testPostData['id']);

        $this->assertEquals($testPostData['id'], $testPost->getId());
        $this->assertEquals($testPostData['message'], $testPost->getMessage());
        $this->assertEquals($testPostData['created_at_timestamp'], $testPost->getCreatedAt()->getTimestamp());
        $this->assertEquals($testPostData['story'], $testPost->getStory());
    }

    private function createPageReader()
    {
        $output = new Output();

        /** @var Facebook $facebook */
        $facebook = $this->createFacebookInstance();

        $pageReader = new PageReader($facebook, $this->getEntityManager(), $output);

        return $pageReader;
    }

    private function createFacebookInstance()
    {
        $fakeFacebook = $this->getMockBuilder(Facebook::class)
                             ->disableOriginalConstructor()
                             ->setMethods(['get'])
                             ->getMock();

        $fakeFacebook->expects($this->any())->method('get')->willReturnCallback([$this, 'facebookGetCallback']);

        return $fakeFacebook;
    }

    public function facebookGetCallback()
    {
        $endpointsXResponse = [
            '/111/posts?pretty=0'                                                => '111.json',
            '/222/posts?limit=100&pretty=0'                                      => '222_0.json',
            '/222/posts?limit=100&after=' . $this->getAfterToken() . '&pretty=0' => '222_1.json',
        ];

        /** @var FacebookRequest $fakeFacebookRequest */
        $fakeFacebookRequest = $this->getMockBuilder(FacebookRequest::class)->disableOriginalConstructor()->getMock();

        $endpoint = func_get_args()[0];

        if (!isset($endpointsXResponse[$endpoint])) {
            throw new Exception(sprintf('Endpoint "%s" was not expected', $endpoint));
        }

        $responseFile = __DIR__ . '/../../assets/ApiResponses/Posts/' . $endpointsXResponse[$endpoint];
        $responseContent = file_get_contents($responseFile);

        $facebookResponse = new FacebookResponse($fakeFacebookRequest, $responseContent);

        return $facebookResponse;
    }

    private function getAfterToken()
    {
        return 'Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5TXhNakEwTmpFek1EUTJOVGt4TkRFNk5EUTNNVE13T0RRMk5EYzBORGszT1RRM01BOE1ZAWEJwWDNOMGIzSjVYMmxrRHlBeE1qQTBOakV6TURRMk5Ua3hOREZAmTVRJNE9UTTVORFU0TVRBNU9URXpOUThFZAEdsdFpRWllUOWU5QVE9PQZDZD';
    }
}
