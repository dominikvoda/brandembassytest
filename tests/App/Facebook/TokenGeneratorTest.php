<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 01.08.17
 * Time: 0:03
 */

namespace App\Facebook\Tests;

use App\Exceptions\FacebookException;
use App\Facebook\TokenGenerator;
use App\TestCase;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use PHPUnit_Framework_MockObject_MockObject;

class TokenGeneratorTest extends TestCase
{
    public function testGenerateTokenSuccess()
    {
        /** @var Facebook|PHPUnit_Framework_MockObject_MockObject $fakeFacebook */
        $fakeFacebook = $this->createFacebookInstance();
        $tokenGenerator = new TokenGenerator($fakeFacebook);

        $fakeResponse = $this->getFacebookResponseFromFile(__DIR__ . '/../../assets/ApiResponses/token.json');
        $fakeFacebook->expects($this->any())->method('get')->willReturn($fakeResponse);

        $this->assertEquals('test', $tokenGenerator->generateToken('whatever'));
    }

    public function testFacebookException()
    {
        $this->expectException(FacebookException::class);

        /** @var Facebook|PHPUnit_Framework_MockObject_MockObject $fakeFacebook */
        $fakeFacebook = $this->createFacebookInstance();
        $tokenGenerator = new TokenGenerator($fakeFacebook);

        $fakeFacebook->expects($this->any())->method('get')->willThrowException(new FacebookSDKException('whatever'));
        $tokenGenerator->generateToken('whatever');
    }

    private function createFacebookInstance()
    {
        $fakeFacebook = $this->getMockBuilder(Facebook::class)
                             ->disableOriginalConstructor()
                             ->setMethods(['get'])
                             ->getMock();

        return $fakeFacebook;
    }

    private function getFacebookResponseFromFile($file)
    {
        /** @var FacebookRequest $fakeFacebookRequest */
        $fakeFacebookRequest = $this->getMockBuilder(FacebookRequest::class)->disableOriginalConstructor()->getMock();

        $responseContent = file_get_contents($file);

        return new FacebookResponse($fakeFacebookRequest, $responseContent);
    }
}
