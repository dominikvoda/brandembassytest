<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 31.07.17
 * Time: 23:53
 */

namespace App\Facebook\Tests;

use App\Exceptions\UnexpectedResultException;
use App\Facebook\PostsResponse;
use App\TestCase;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;

class PostsResponseTest extends TestCase
{
    public function testValidContent()
    {
        $file = __DIR__ . '/../../assets/ApiResponses/Posts/111.json';
        $facebookResponse = $this->getFacebookResponseFromFile($file);

        $postsResponse = new PostsResponse($facebookResponse);

        $expected = 'Q2c4U1pXNTBYM0YxWlhKNVgzTjBiM0o1WDJsa0R5RXlPRFkwT0RNd09EVTFNalk2TFRVMk1ESTJOekF6TURZAMU5UTTJNREl5TmpBUERHRndhVjl6ZAEc5eWVWOXBaQThlTWpnMk5EZA3pNRGcxTlRJMlh6RXdNVFU0TXpVeU1qQTBNRFF3TlRJM0R3UjBhVzFsQmxqYmdod0IZD';

        $this->assertEquals($expected, $postsResponse->getAfter());
        $this->assertEquals(25, count($postsResponse->getPosts()));
    }

    public function testUnexpectedResultException()
    {
        $this->expectException(UnexpectedResultException::class);
        $file = __DIR__ . '/../../assets/ApiResponses/Posts/222_0_broken.json';
        $facebookResponse = $this->getFacebookResponseFromFile($file);

        $postsResponse = new PostsResponse($facebookResponse);
        $postsResponse->getAfter();
    }

    private function getFacebookResponseFromFile($file)
    {
        /** @var FacebookRequest $fakeFacebookRequest */
        $fakeFacebookRequest = $this->getMockBuilder(FacebookRequest::class)->disableOriginalConstructor()->getMock();

        $responseContent = file_get_contents($file);

        return new FacebookResponse($fakeFacebookRequest, $responseContent);
    }
}
