<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 01.08.17
 * Time: 0:35
 */

namespace App\Services\Tests;

use App\Entities\Page;
use App\Entities\Post;
use App\Facebook\PageReader;
use App\Services\PageProcessor;
use App\TestCase;
use Doctrine\Common\Collections\ArrayCollection;

class PageProcessorTest extends TestCase
{
    const TEST_PAGE_ID = '286483085526';
    const TEST_POST_ID = '286483085526_10158578698710527';

    public function setUp()
    {
        parent::setUp();
        $this->getEntityManager()->beginTransaction();
    }

    public function tearDown()
    {
        $this->getEntityManager()->rollback();
    }

    public function testExecute()
    {
        $syncTimestamp = new \DateTime();
        $page = $this->getTestPage();

        $this->assertEquals('2017-08-02', $page->getSyncedAt()->format('Y-m-d'));
        $this->assertEquals(3, $page->getPosts()->count());

        /** @var PageReader $fakeFacebookPageReader */
        $fakeFacebookPageReader = $this->createFacebookPageReaderInstance();

        $pageProcessor = new PageProcessor($fakeFacebookPageReader, $this->getEntityManager());

        $pageProcessor->execute($page);

        $this->assertTrue($page->getSyncedAt()->getTimestamp() >= $syncTimestamp->getTimestamp());
        $this->assertEquals($this->getPost('TEST')->getPage(), $page);
        $this->assertEquals('updated', $this->getPost(self::TEST_POST_ID)->getMessage());
    }

    private function getTestPage(): Page
    {
        $pageRepository = $this->getEntityManager()->getRepository(Page::class);

        /** @var Page $page */
        $page = $pageRepository->find(self::TEST_PAGE_ID);

        return $page;
    }

    private function createFacebookPageReaderInstance()
    {
        $fakeFacebook = $this->getMockBuilder(PageReader::class)
                             ->disableOriginalConstructor()
                             ->setMethods(['getPosts'])
                             ->getMock();

        $fakeFacebook->expects($this->any())->method('getPosts')->willReturnCallback([$this, 'getPostsCallback']);

        return $fakeFacebook;
    }

    private function getPost($id): Post
    {
        $postRepository = $this->getEntityManager()->getRepository(Post::class);

        /** @var Post $post */
        $post = $postRepository->find($id);

        return $post;
    }

    public function getPostsCallback()
    {
        $newPost = new Post();
        $newPost->setCreatedAt(new \DateTime());
        $newPost->setMessage('test message');
        $newPost->setId('TEST');

        $updatedPost = $this->getPost(self::TEST_POST_ID);
        $updatedPost->setMessage('updated');

        return new ArrayCollection([$newPost, $updatedPost]);
    }
}
