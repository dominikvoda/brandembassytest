<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 27.07.17
 * Time: 23:46
 */

namespace App\Entities\Tests;

use App\Entities\Page;
use App\Entities\Post;
use App\Exceptions\InvalidFacebookPostException;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    public function testCreateFromFacebookPost()
    {
        $tmpPage = new Page();
        $tmpPage->setId(123456);

        $postArray = [
            'id'           => 1234,
            'message'      => 'test message',
            'created_time' => '2017-07-27T14:36:02+0000',
            'story'        => 'some cool story',
        ];

        $post = Post::createFromFacebookPost($postArray);
        $createdAt = new \DateTime($postArray['created_time']);

        $post->setPage($tmpPage);

        $this->assertEquals($postArray['id'], $post->getId());
        $this->assertEquals($postArray['message'], $post->getMessage());
        $this->assertEquals($postArray['story'], $post->getStory());
        $this->assertEquals($createdAt->getTimestamp(), $post->getCreatedAt()->getTimestamp());
        $this->assertEquals($tmpPage, $post->getPage());
    }

    public function testInvalidFacebookPostDataException(){
        $this->expectException(InvalidFacebookPostException::class);
        $postArray = [
            'id'           => 1234,
            'created_time' => '2017-07-27T14:36:02+0000',
            'story'        => 'some cool story',
        ];

        Post::createFromFacebookPost($postArray);
    }
}
