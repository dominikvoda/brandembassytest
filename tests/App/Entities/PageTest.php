<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 27.07.17
 * Time: 23:57
 */

namespace App\Entities\Tests;

use App\Entities\Page;
use App\Entities\Post;
use PHPUnit\Framework\TestCase;

class PageTest extends TestCase
{

    public function testQueueMethods()
    {
        $synced = new \DateTime();

        $page = new Page();
        $page->setId(1234);
        $page->setActive(true);
        $page->setDescription('description');
        $page->setInQueue(false);
        $page->setPeriod(60);
        $page->setTitle('title');

        $this->assertEquals(1234, $page->getId());
        $this->assertEquals(true, $page->getActive());
        $this->assertFalse($page->inQueue());

        $page->queued();
        $this->assertTrue($page->inQueue());

        $page->synced($synced);
        $this->assertEquals($synced->getTimestamp(), $page->getSyncedAt()->getTimestamp());

        $page->reject();
        $this->assertFalse($page->inQueue());

        $this->assertEquals('description', $page->getDescription());
        $this->assertEquals('title', $page->getTitle());

        $this->assertTrue($page->getPosts()->isEmpty());

        $testPost = $this->createSimplePost();
        $page->addPost($testPost);

        $this->assertFalse($page->getPosts()->isEmpty());

        $this->assertEquals(1, $page->getPosts()->count());
        $this->assertEquals($testPost, $page->getPosts()->first());

        $page->removePost($testPost);
        $this->assertTrue($page->getPosts()->isEmpty());
    }

    private function createSimplePost(){
        $post = new Post();
        return $post;
    }
}
