<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 27.07.17
 * Time: 22:22
 */

namespace App\Utils\Tests;

use App\Utils\Http;
use PHPUnit\Framework\TestCase;

class HttpTest extends TestCase
{

    public function testCreateQueryString()
    {
        $url = 'test.com';
        $tests = [
            'test.com'                 => null,
            'test.com?foo=bar'         => [
                'foo' => 'bar',
            ],
            'test.com?foo=bar&bar=foo' => [
                'foo' => 'bar',
                'bar' => 'foo',
            ],
        ];

        foreach ($tests as $expected => $parameters) {
            if ($parameters) {
                $this->assertEquals($expected, Http::createQueryString($url, $parameters));
            }
            else{
                $this->assertEquals($expected, Http::createQueryString($url));
            }
        }
    }
}
