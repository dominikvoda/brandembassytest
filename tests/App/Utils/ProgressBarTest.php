<?php
/**
 * Created by PhpStorm.
 * User: dominik
 * Date: 07.07.17
 * Time: 13:18
 */

namespace App\Utils;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\Output;
use App\Utils\ProgressBar;

class ProgressBarTest extends TestCase
{
    public function testConstructor()
    {
        $consoleOutput = new TestOutput();
        $simpleProgressBar = new ProgressBar($consoleOutput);

        $this->assertAttributeEquals($consoleOutput, 'output', $simpleProgressBar);
        $this->assertAttributeEquals($this->getDefaultFormatString(), 'format', $simpleProgressBar);
        $this->assertAttributeInstanceOf(
            \Symfony\Component\Console\Helper\ProgressBar::class,
            'masterProgressBar',
            $simpleProgressBar
        );
    }

    public function testFullConstructor()
    {
        $title = 'Test progress bar';

        $consoleOutput = new TestOutput();
        $fullProgressBar = new ProgressBar($consoleOutput, 200000, $title);
        $this->assertAttributeEquals($consoleOutput, 'output', $fullProgressBar);
        $this->assertAttributeEquals($title . $this->getDefaultFormatString(), 'format', $fullProgressBar);
        $this->assertAttributeEquals(true, 'started', $fullProgressBar);

        $this->assertFalse($fullProgressBar->start(0));

        $this->assertEquals(0, $fullProgressBar->getProgress());

        $fullProgressBar->advance();

        $this->assertEquals(1, $fullProgressBar->getProgress());

        $fullProgressBar->finish();
        $this->assertStringEndsWith(PHP_EOL, $consoleOutput->output);
    }

    public function testInvalidProgressBar()
    {
        $consoleOutput = new TestOutput();
        $progressBar = new ProgressBar($consoleOutput);
        $this->assertAttributeEquals(false, 'started', $progressBar);

        $progressBar->advance();
        $this->assertEquals('ProgressBar not stated yet' . PHP_EOL, $consoleOutput->output);
    }

    private function getDefaultFormatString()
    {
        return ' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%';
    }
}


class TestOutput extends Output
{
    public $output = '';

    public function clear()
    {
        $this->output = '';
    }

    protected function doWrite($message, $newline)
    {
        $this->output .= $message . ($newline ? "\n" : '');
    }
}
