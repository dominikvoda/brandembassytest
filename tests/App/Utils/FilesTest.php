<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 27.07.17
 * Time: 2:54
 */

namespace App\Utils\Tests;

use App\Utils\Files;
use PHPUnit\Framework\TestCase;

class FilesTest extends TestCase
{
    public function testFileExists()
    {
        $fileName = $this->getTestFilePath(md5(time()));

        $this->assertFileNotExists($fileName);

        $this->assertFalse(Files::fileExist($fileName));

        file_put_contents($fileName, 'empty');

        $this->assertTrue(Files::fileExist($fileName));

        unlink($fileName);

        $this->assertFalse(Files::fileExist($fileName));
    }

    public function testCreateDirectoryIfNotExist()
    {
        $directoryName = $this->getTestDirectoryPath(md5(time()));

        $this->assertDirectoryNotExists($directoryName);

        Files::createDirectoryIfNotExist($directoryName);

        $this->assertDirectoryExists($directoryName);

        rmdir($directoryName);

        $this->assertDirectoryNotExists($directoryName);
    }

    private function getTestFilePath($randomFileName)
    {
        return __DIR__ . '/../../assets/TmpFiles/' . $randomFileName . '.tmp';
    }

    private function getTestDirectoryPath($directoryName)
    {
        return __DIR__ . '/../../assets/TmpFiles/' . $directoryName;
    }
}
