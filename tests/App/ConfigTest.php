<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 30.07.17
 * Time: 0:21
 */

namespace App\Tests;

use App\Config;
use App\Exceptions\ConfigException;
use App\Exceptions\ConfigValidationException;
use PHPUnit\Framework\TestCase;

/**
 * @runTestsInSeparateProcesses
 */
class ConfigTest extends TestCase
{
    const TEST_CONSTANT = 'imconstant';


    public function testNotSetException()
    {
        $this->expectException(ConfigException::class);
        Config::get('whatever');
    }

    public function testLoadInvalidConfigFile()
    {
        $this->expectException(ConfigException::class);
        Config::loadConfigFile('not-existing-file.whatever');
    }

    public function testBasicAccess()
    {
        Config::loadConfigFile($this->getTestConfigFile(), []);

        $this->assertEquals(self::TEST_CONSTANT, Config::getConstantValue('test.constant'));
        $this->assertArrayHasKey('value1', Config::get('test'));
        $this->assertArrayHasKey('value2', Config::get('test'));

        $this->assertArraySubset(['deeper' => ['deepest' => ':)']], Config::get('test.deep'));
        $this->assertArraySubset(['test' => ['value1' => 1, 'value2' => 2]], Config::get());

        $this->assertTrue(Config::validate(['deeper'], 'test.deep'));
    }

    public function testInvalidConstantException()
    {
        $this->expectException(ConfigException::class);
        Config::loadConfigFile($this->getTestConfigFile(), []);
        Config::getConstantValue('test.not_constant');
    }

    public function testValueNotFoundException()
    {
        $this->expectException(ConfigException::class);
        Config::loadConfigFile($this->getTestConfigFile(), []);
        Config::getConstantValue('not.existed.staffs');
    }

    public function testRequiredValuesNotSet()
    {
        Config::loadConfigFile($this->getTestConfigFile(), []);
        $this->expectException(ConfigValidationException::class);
        Config::get('test.deep', ['not_so_deep']);
    }

    public function testGetConfigFile()
    {
        Config::loadConfigFile($this->getTestConfigFile(), []);
        $this->assertEquals($this->getTestConfigFile(), Config::getConfigFile());
    }


    public function testUnableToOverwriteConfigException()
    {
        $this->expectException(ConfigException::class);
        Config::loadConfigFile($this->getTestConfigFile(), []);
        Config::loadConfigFile('not-existing-file.whatever');
    }

    private function getTestConfigFile()
    {
        return __DIR__ . '/../assets/config/test.yml';
    }
}
