-- Adminer 4.2.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `period` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `synced_at` datetime DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `in_queue` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `page` (`id`, `title`, `period`, `active`, `synced_at`, `description`, `in_queue`) VALUES
('120461304659141',	'O2',	30,	1,	'2017-07-27 06:14:53',	NULL,	0),
('176688316811',	'Vodafone',	60,	1,	'2017-07-27 06:14:54',	NULL,	0),
('286483085526',	'TMobile',	20,	1,	'2017-07-27 06:14:54',	NULL,	0);

DROP TABLE IF EXISTS `post`;
CREATE TABLE `post` (
  `id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `story` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_5A8A6C8DC4663E4` (`page_id`),
  CONSTRAINT `FK_5A8A6C8DC4663E4` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `post` (`id`, `page_id`, `message`, `created_at`, `story`) VALUES
('120461304659141_1298217640216829',	'120461304659141',	'Taky\npíšete\nzprávy\ntakhle? :D',	'2016-12-22 19:00:00',	NULL),
('120461304659141_1298925556812704',	'120461304659141',	'Krásné Vánoce <3\n\nPS: Posíláme výběr nejlepších vánočních songů na Spotify: www.o2.cz/osobni/pf-2017/',	'2016-12-23 14:03:39',	'O2 CZ updated their cover photo.'),
('120461304659141_1298931403478786',	'120461304659141',	'Tak jo, už na nás jde vánoční nálada :) Co u vás?',	'2016-12-23 14:15:43',	'O2 CZ shared Johny Machette\'s video.'),
('120461304659141_1299723030066290',	'120461304659141',	'Tak co vám Ježíšek naježil? ',	'2016-12-24 19:01:49',	'O2 CZ was live.'),
('286483085526_10158578698710527',	'286483085526',	'V období 12. 5. od 20:00 až 14. 5. potřebujeme promazat pár koleček, aby vše bylo jako zamlada. Vy můžete dál volat, SMSkovat i datovat, jak jste zvyklí, ale pokud si\npotřebujete aktivovat třeba datový balíček nebo roaming, je dobré to udělat do pátku. Více informací najdete na t-mobile.cz/odstavka.',	'2017-05-11 16:52:27',	NULL),
('286483085526_10158603864655527',	'286483085526',	'Teď už můžete bez obav datovat v EU za stejné ceny jako doma. Sbalte si data na dovolenou! Více na t-mobile.cz/tarify.',	'2017-05-16 17:32:59',	NULL),
('286483085526_10158644119230527',	'286483085526',	'Už máte plány na 4.–6. 7.? Vyhrajte 2 x 2 lístky na Rock for People či 10 x 3GB dat v aplikaci Můj T-Mobile. Užijte si léto bez zábran!\nAplikaci stahujte na tmo.cz/mtm2',	'2017-05-24 15:10:56',	NULL);

-- 2017-08-01 03:49:06
