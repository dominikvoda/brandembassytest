<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 30.07.17
 * Time: 1:42
 */

namespace App\Tests;

use App\Services\Output\IOutput;

class Output implements IOutput
{
    public $output = '';

    public function clear()
    {
        $this->output = '';
    }

    public function writeLn(string $message)
    {
        $this->doWrite($message, true);
    }

    public function error(string $message)
    {
        $this->doWrite($message, true);
    }

    public function warning(string $message)
    {
        $this->doWrite($message, true);
    }

    public function success(string $message)
    {
        $this->doWrite($message, true);
    }

    public function debug(string $message)
    {
        $this->doWrite($message, true);
    }

    public function printExceptionTrace(\Exception $e, string $trace = '', $index = 0)
    {
        $this->doWrite($e->getMessage(), true);
    }

    protected function doWrite($message, $newline)
    {
        $this->output .= $message . ($newline ? "\n" : '');
    }
}