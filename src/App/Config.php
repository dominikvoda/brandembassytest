<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 28.07.17
 * Time: 15:46
 */

namespace App;

use App\Exceptions\ConfigException;
use App\Exceptions\ConfigValidationException;
use App\Utils\Files;
use Symfony\Component\Yaml\Yaml;

class Config
{
    /**
     * @var bool
     */
    protected static $loaded = false;

    /**
     * @var null|string
     */
    protected static $configFile = null;

    /** @var  array */
    protected static $config;

    /**
     * @param string|null $path
     *
     * @return array|mixed
     */
    public static function get(string $path = null, $required = [])
    {
        if (!self::$loaded) {
            throw new ConfigException('Config was not loaded yet, please run Config::loadConfigFile');
        }

        if (count($required) !== 0) {
            self::validate($required, $path);
        }

        if (null === $path) {
            return self::$config;
        }

        return self::getChildrenValue($path);
    }


    /**
     * @param string $file
     * @param string $children
     *
     * @return mixed
     */
    public static function getConstantValue(string $childrenPath)
    {
        $value = self::get($childrenPath);

        if (defined($value)) {
            return constant($value);
        }

        throw new ConfigException(sprintf('Value "%s" is not constant', $childrenPath));
    }

    /**
     * @param string $file
     */
    public static function loadConfigFile(
        string $file,
        array $required = ['database', 'sync', 'facebook', 'logs', 'queues']
    )
    {
        if (false === self::$loaded) {
            if (Files::fileExist($file)) {
                self::$config = self::loadConfig($file);
                self::$loaded = true;
                if (self::validate($required)) {
                    self::$configFile = $file;

                    return;
                }
            } else {
                throw new ConfigException(sprintf('File "%s" not exist', $file));
            }
        }
        throw new ConfigException('Config file is already loaded, you can\' overwrite this settings');
    }

    /**
     * @param array       $required
     * @param string      $configFile
     * @param null|string $childrenPath
     *
     * @return bool
     */
    public static function validate(array $required, string $childrenPath = null): bool
    {
        $missing = [];
        $valid = true;

        $configSubset = self::get($childrenPath);

        foreach ($required as $key) {
            if (!array_key_exists($key, $configSubset) || null === $configSubset[$key]) {
                $missing[] = $key;
                $valid = false;
            }
        }

        if ($valid) {
            return true;
        }

        throw new ConfigValidationException(
            sprintf('Config file "%s", path: "%s" missing following key/s:', self::$configFile, $childrenPath), $missing
        );
    }

    /**
     * @return null|string
     */
    public static function getConfigFile()
    {
        return self::$configFile;
    }

    /**
     * @param array       $config
     * @param null|string $childrenPath
     *
     * @return array|mixed
     */
    private static function getChildrenValue(string $childrenPath)
    {
        $path = explode('.', $childrenPath);

        $children = self::$config;

        foreach ($path as $item) {
            if (isset($children[$item])) {
                $children = $children[$item];
            } else {
                throw new ConfigException(
                    sprintf('Config value not found (%s) in config file "%s"', $childrenPath, self::$configFile)
                );
            }
        }

        return $children;
    }

    /**
     * @param $file
     *
     * @return mixed
     */
    private static function loadConfig($file)
    {
        return Yaml::parse(file_get_contents($file));
    }
}