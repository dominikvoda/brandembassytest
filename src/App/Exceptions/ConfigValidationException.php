<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 27.07.17
 * Time: 1:34
 */

namespace App\Exceptions;

use Throwable;

class ConfigValidationException extends ConfigException
{
    public function __construct(
        string $message = "",
        array $missingValues = [],
        int $code = 0,
        Throwable $previous = null
    )
    {
        $message .= sprintf('[%s]', join(', ', $missingValues));
        parent::__construct($message, $code, $previous);
    }
}