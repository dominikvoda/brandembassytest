<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 15.07.17
 * Time: 21:33
 */

namespace App\Exceptions;

use Throwable;

class InvalidFacebookPostException extends FacebookException
{
    public function __construct($message = "", $postArray, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->message = $message . $this->printPostArray($postArray);
    }

    protected function printPostArray($postArray)
    {
        $result = ':' . PHP_EOL;

        foreach ($postArray as $key => $value) {
            $result .= str_pad($key, 15, ' ', STR_PAD_RIGHT) . ' => ' . $value . PHP_EOL;
        }

        $result .= PHP_EOL;

        return $result;
    }
}