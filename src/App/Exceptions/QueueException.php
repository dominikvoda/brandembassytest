<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 24.07.17
 * Time: 23:40
 */

namespace App\Exceptions;

use Zend\Code\Exception\RuntimeException;

class QueueException extends RuntimeException
{

}