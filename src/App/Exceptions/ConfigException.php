<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 14:44
 */

namespace App\Exceptions;

class ConfigException extends \RuntimeException
{
    protected $configFile;

    public function addFileInfo($file)
    {
        $this->message .= ' in file (' . $file . ')';
    }
}