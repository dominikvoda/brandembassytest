<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 15.07.17
 * Time: 22:40
 */

namespace App\Repositories;

use App\Entities\Page;
use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function findNotSyncedPages()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->where('timestampdiff(minute, p.syncedAt, now()) > p.period OR p.syncedAt IS NULL');
        $qb->andWhere('p.active = 1');

        return $qb->getQuery()->execute();
    }

    /**
     * @param $title
     *
     * @return null|Page|object
     */
    public function findByTitle(string $title): Page
    {
        return $this->findOneBy(
            [
                'title'  => $title,
                'active' => 1,
            ]
        );
    }

    /**
     * @return array
     */
    public function findAllActive()
    {
        return $this->findBy(['active' => true]);
    }

    /**
     *  Set all pages "not in queue"
     */
    public function resetQueueLocks()
    {
        $qb = $this->createQueryBuilder('p');
        $qb->update();
        $qb->set('p.inQueue', 0);
        $qb->getQuery()->execute();
    }
}