<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 13.07.17
 * Time: 11:22
 */

namespace App\Repositories;

use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    /**
     * @param array $ids
     *
     * @return array
     */
    public function findPostByIds(array $ids): array
    {
        return $this->findBy(['id' => $ids]);
    }
}