<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 17.07.17
 * Time: 14:02
 */

namespace App\Services;

use App\Entities\Page;
use App\Entities\Post;
use App\Facebook\PageReader;
use App\Services\Output\IOutput;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Facebook\Facebook;

class PageProcessor
{
    /**
     *  Posts read for init call
     */
    const INIT_LIMIT = 200;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var PageReader
     */
    private $facebookPageReader;


    /**
     * PageProcessor constructor.
     *
     * @param PageReader    $facebookPageReader
     * @param EntityManager $entityManager
     */
    public function __construct(PageReader $facebookPageReader, EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->facebookPageReader = $facebookPageReader;
    }

    /**
     * @param Page $page
     */
    public function execute(Page $page)
    {
        $syncTimestamp = new \DateTime();

        $posts = $this->fetchPostsFromFacebook($page);

        $this->processPosts($posts, $page);
        $this->syncPage($page, $syncTimestamp);
    }

    /**
     * @param ArrayCollection $posts
     * @param Page            $page
     */
    protected function processPosts(ArrayCollection $posts, Page $page)
    {
        /** @var Post $post */
        foreach ($posts as $post) {
            $post->setPage($page);
            $this->getEntityManager()->persist($post);
        }
    }

    /**
     * @param Page $page
     *
     * @return ArrayCollection
     */
    protected function fetchPostsFromFacebook(Page $page): ArrayCollection
    {
        if (null === $page->getSyncedAt()) {
            return $this->getFacebookPageReader()->getPosts($page, self::INIT_LIMIT);
        }

        return $this->getFacebookPageReader()->getPosts($page);
    }

    /**
     * @param Page      $page
     * @param \DateTime $dateTime
     */
    protected function syncPage(Page $page, \DateTime $dateTime)
    {
        $page->synced($dateTime);
        $this->getEntityManager()->persist($page);
        $this->getEntityManager()->flush();
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    /**
     * @return PageReader
     */
    protected function getFacebookPageReader(): PageReader
    {
        return $this->facebookPageReader;
    }
}