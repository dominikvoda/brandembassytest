<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 0:44
 */

namespace App\Services\Output;

use App\Config;
use Symfony\Component\Console\Style\SymfonyStyle;

class SymfonyConsoleOutput implements IOutput
{
    const LEVEL_DEBUG = 1;
    const LEVEL_INFO = 2;
    const LEVEL_WARNING = 3;
    const LEVEL_ERROR = 4;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var int
     */
    private $logLevel;

    /**
     * SymfonyConsoleOutput constructor.
     *
     * @param SymfonyStyle $symfonyStyle
     */
    public function __construct(SymfonyStyle $symfonyStyle)
    {
        $this->io = $symfonyStyle;
        $this->logLevel = $this->getLogLevel();
    }

    /**
     * @param string $message
     */
    public function writeLn(string $message)
    {
        if ($this->isPrintable(self::LEVEL_INFO)) {
            $this->io->writeln($this->prependLogTime($message));
        }
    }

    /**
     * @param string $message
     */
    public function error(string $message)
    {
        if ($this->isPrintable(self::LEVEL_ERROR)) {
            $this->io->error($this->prependLogTime($message));
        }
    }

    /**
     * @param string $message
     */
    public function warning(string $message)
    {
        if ($this->isPrintable(self::LEVEL_WARNING)) {
            $this->io->warning($this->prependLogTime($message));
        }
    }

    /**
     * @param string $message
     */
    public function success(string $message)
    {
        if ($this->isPrintable(self::LEVEL_INFO)) {
            $this->io->success($this->prependLogTime($message));
        }
    }

    /**
     * @param string $message
     */
    public function debug(string $message)
    {
        if ($this->isPrintable(self::LEVEL_DEBUG)) {
            $this->writeLn('DEBUG: ' . $message);
        }
    }

    public function printExceptionTrace(\Exception $e, string $trace = '', $index = 0)
    {
        $this->error($this->getExceptionTraceMessage($e));
    }

    private function getExceptionTraceMessage(\Exception $e, string $trace = '', $index = 0)
    {
        $trace .= PHP_EOL . '#' . $index . ': ' . $e->getMessage();
        if ($e->getPrevious()) {
            return $this->getExceptionTraceMessage($e->getPrevious(), $trace, ++$index);
        }

        return $trace;
    }

    /**
     * @param $message
     *
     * @return string
     */
    private function prependLogTime($message): string
    {
        return $this->getLogTime() . ' ' . $message;
    }

    /**
     * @return string
     */
    private function getLogTime(): string
    {
        return date('Y-m-d H:i:s');
    }

    /**
     * @return int
     */
    private function getLogLevel(): int
    {
        return Config::getConstantValue('logs.console.level');
    }

    /**
     * @param int $level
     *
     * @return bool
     */
    private function isPrintable(int $level): bool
    {
        return $level >= $this->logLevel;
    }

}