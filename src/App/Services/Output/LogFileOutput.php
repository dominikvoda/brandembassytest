<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 13:44
 */

namespace App\Services\Output;

use App\Config;
use App\Utils\Files;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class LogFileOutput implements IOutput
{
    /** @var Logger */
    private $monolog;

    /**
     * LogFileOutput constructor.
     *
     * @param string $type
     */
    public function __construct($type = 'app_log')
    {
        $level = Config::getConstantValue('logs.file_log.level');
        $this->monolog = new Logger($type);
        $this->monolog->pushHandler(new StreamHandler($this->getLogFile($type), $level));
    }

    /**
     * @param string $message
     */
    public function writeLn(string $message)
    {
        $this->monolog->debug($message);
    }

    /**
     * @param string $message
     */
    public function error(string $message)
    {
        $this->monolog->error($message);
    }

    /**
     * @param string $message
     */
    public function warning(string $message)
    {
        $this->monolog->warning($message);
    }

    /**
     * @param string $message
     */
    public function success(string $message)
    {
        $this->monolog->info($message);
    }

    /**
     * @param string $message
     */
    public function debug(string $message)
    {
        $this->monolog->debug($message);
    }

    public function printExceptionTrace(\Exception $e, string $trace = '', $index = 0)
    {
        $this->error($this->getExceptionTraceMessage($e));
    }

    private function getExceptionTraceMessage(\Exception $e, string $trace = '', $index = 0)
    {
        $trace .= PHP_EOL . '#' . $index . ': ' . $e->getMessage();
        if ($e->getPrevious()) {
            return $this->getExceptionTraceMessage($e->getPrevious(), $trace, ++$index);
        }

        return $trace;
    }

    /**
     * @param string $type
     *
     * @return string
     */
    private function getLogFile(string $type): string
    {
        $configDirectory = Config::get('logs.file_log.directory');
        $directory = __DIR__ . '/../../../' . $configDirectory . '/' . $type;

        Files::createDirectoryIfNotExist($directory);

        return $directory . '/' . date('Y-m-d') . '.log';
    }

}