<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 0:43
 */

namespace App\Services\Output;

interface IOutput
{
    /**
     * @param string $message
     *
     * @return void
     */
    public function writeLn(string $message);

    /**
     * @param string $message
     *
     * @return void
     */
    public function error(string $message);

    /**
     * @param string $message
     *
     * @return void
     */
    public function warning(string $message);

    /**
     * @param string $message
     *
     * @return void
     */
    public function success(string $message);

    /**
     * @param string $message
     *
     * @return void
     */
    public function debug(string $message);

    /**
     * @param \Exception $e
     *
     * @return mixed
     */
    public function printExceptionTrace(\Exception $e, string $trace = '', $index = 0);
}