<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 18.07.17
 * Time: 15:23
 */

namespace App\Services\Queue;

use App\Services\Output\IOutput;

interface IMessageQueue
{
    /**
     * IMessageQueue constructor.
     *
     * @param string $queueName
     */
    public function __construct(string $queueName);

    /**
     * @param string $message
     * @param array  $properties
     *
     * @return void
     */
    public function publishMessage(string $message, $properties = []);

    /**
     * @param callable $callback
     * @param IOutput  $output
     *
     * @return void
     */
    public function consumeMessages(callable $callback, IOutput $output);

    /**
     * @return void
     */
    public function close();
}