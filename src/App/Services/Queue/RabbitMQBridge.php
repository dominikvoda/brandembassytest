<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 18.07.17
 * Time: 15:22
 */

namespace App\Services\Queue;

use App\Config;
use App\Services\Output\IOutput;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQBridge implements IMessageQueue
{
    /**
     * @var \PhpAmqpLib\Channel\AMQPChannel
     */
    private $channel;

    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @var string
     */
    private $queue;

    /**
     * RabbitMQBridge constructor.
     *
     * @param string $queueName
     */
    public function __construct(string $queueName)
    {
        $this->connection = $this->createConnection();
        $this->channel = $this->connection->channel();
        $this->queue = $queueName;
        $this->declareQueue($queueName);
    }


    /**
     * @param string $message
     * @param array  $properties
     */
    public function publishMessage(string $message, $properties = [])
    {
        $defaultProperties = [
            'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT,
        ];

        if (!$message instanceof AMQPMessage) {
            $message = new AMQPMessage($message, array_merge($defaultProperties, $properties));
        }

        $this->channel->basic_publish($message, '', $this->queue);
    }

    /**
     * @param callable $callback
     * @param IOutput  $output
     */
    public function consumeMessages(callable $callback, IOutput $output)
    {
        $this->channel->basic_qos(null, 1, null);
        $this->channel->basic_consume($this->queue, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }

        $this->close();
    }

    /**
     * @param string $queue
     */
    private function declareQueue(string $queue)
    {
        $this->channel->queue_declare($queue, false, true, false, false);
    }

    /**
     * @return void
     */
    public function close()
    {
        $this->channel->close();
        $this->connection->close();
    }

    /**
     * @return AMQPStreamConnection
     */
    private final function createConnection(): AMQPStreamConnection
    {
        $connection = Config::get('queues.rabbitmq.connection');

        return new AMQPStreamConnection(
            $connection['host'], $connection['port'], $connection['user'], $connection['password']
        );
    }
}