<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 24.07.17
 * Time: 22:22
 */

namespace App\Services\Queue;

use App\Config;
use App\Exceptions\QueueException;
use App\Services\Output\IOutput;
use App\Utils\Files;
use Doctrine\Common\Collections\ArrayCollection;

class SimpleQueue implements IMessageQueue
{
    /**
     * @var string
     */
    private $queue;

    /**
     * @var string
     */
    private $messagesDirectory;

    /**
     * @var ArrayCollection
     */
    private $messages;

    /** @var  IOutput */
    private $output;

    /**
     * SimpleQueue constructor.
     *
     * @param string $queueName
     */
    public function __construct(string $queueName)
    {
        $this->queue = $queueName;
        $this->messagesDirectory = $this->resolveMessagesDirectory($queueName);
        $this->messages = new ArrayCollection();
    }

    /**
     * @param string $message
     * @param array  $properties
     */
    public function publishMessage(string $message, $properties = [])
    {
        $this->appendMessage($message);
    }

    /**
     * @param callable $callback
     * @param IOutput  $output
     */
    public function consumeMessages(callable $callback, IOutput $output)
    {
        $this->output = $output;

        while (true) {
            $files = $this->loadMessageFiles($this->messagesDirectory);

            if (count($files) === 0) {
                $output->debug('No messages files found');
            }

            foreach ($files as $file) {
                $this->resolveFileMessages($file, $callback);
            }

            sleep(Config::get('queues.simple_queue.tick'));
        }
    }

    /**
     * @param string $fileName
     *
     * @return bool
     */
    protected function createNewMessagesFile(string $fileName): bool
    {
        $content = json_encode($this->getMessages()->toArray());

        return false !== file_put_contents($fileName, $content);
    }

    /**
     * @param string   $file
     * @param callable $callback
     */
    private function resolveFileMessages(string $file, callable $callback)
    {
        $messages = $this->loadMessages($file);

        foreach ($messages as $message) {
            if (call_user_func($callback, $message)) {
                $messages->removeElement($message);
            };
        }

        if ($messages->isEmpty()) {
            $this->removeEmptyMessagesFile($file);
        } else {
            $this->updateMessagesFile($file, $messages);
        }
    }

    /**
     * @return void
     */
    public function close()
    {
        $fileName = $this->getMessagesDirectory() . '/' . time() . '.json';
        if (!$this->createNewMessagesFile($fileName)) {
            throw new QueueException('Can\'t write into queue file (' . $fileName . ')');
        }
    }

    /**
     * @param string $queue
     *
     * @return string
     */
    private function resolveMessagesDirectory(string $queue): string
    {
        $messageDirectory = Config::get('queues.simple_queue.message_directory');

        $directory = __DIR__ . '/../../../' . $messageDirectory . '/' . $queue;

        Files::createDirectoryIfNotExist($directory);

        return $directory;
    }

    /**
     * @return string
     */
    private function getMessagesDirectory(): string
    {
        return $this->messagesDirectory;
    }

    /**
     * @param $file
     *
     * @return array
     */
    private function getMessagesFromFile($file): array
    {
        $messages = file_get_contents($file);

        return json_decode($messages, true);
    }

    /**
     * @param $message
     */
    private function appendMessage($message)
    {
        $this->getMessages()->add($message);
    }

    /**
     * @return ArrayCollection
     */
    private function getMessages(): ArrayCollection
    {
        return $this->messages;
    }

    /**
     * @param string          $messagesFile
     * @param ArrayCollection $messages
     *
     * @return bool
     */
    private function updateMessagesFile(string $messagesFile, ArrayCollection $messages): bool
    {
        $content = json_encode($messages->toArray());

        return false !== file_put_contents($messagesFile, $content);
    }

    /**
     * @param string $file
     *
     * @return ArrayCollection
     */
    private function loadMessages(string $file): ArrayCollection
    {
        return new ArrayCollection($this->getMessagesFromFile($file));
    }

    /**
     * @param string $messagesDirectory
     *
     * @return array
     */
    private function loadMessageFiles(string $messagesDirectory): array
    {
        $timestamps = [];

        if ($handle = opendir($messagesDirectory)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $parts = explode('.', $entry);
                    $timestamps[] = (int)$parts[0];
                }
            }
            closedir($handle);
        }
        sort($timestamps);

        $directory = $this->getMessagesDirectory();

        return array_map(
            function ($timestamp) use ($directory) {
                return $directory . '/' . $timestamp . '.json';
            },
            $timestamps
        );
    }

    /**
     * @param string $file
     */
    private function removeEmptyMessagesFile(string $file)
    {
        if (is_file($file)) {
            unlink($file);
            $this->output->debug('Tmp messages file (' . $file . ') was removed');
        } else {
            $this->output->warning('Tmp messages file (' . $file . ') not found');
        }
    }
}