<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 24.07.17
 * Time: 22:52
 */

namespace App\Services\Queue\Page;

use App\Config;
use App\Entities\Page;
use App\Facebook\PageReader;
use App\Services\Output\IOutput;
use App\Services\PageProcessor;
use Doctrine\ORM\EntityManager;
use Facebook\Facebook;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQPageCallback implements IPageCallback
{
    /**
     * @param EntityManager $entityManager
     * @param IOutput       $output
     *
     * @return callable
     */
    public static function get(EntityManager $entityManager, IOutput $output): callable
    {
        /**
         * @param AMQPMessage $message
         */
        $callback = function ($message) use ($entityManager, $output) {
            $output->writeLn('Received message ' . $message->body);

            $messageData = json_decode($message->body, true);

            $pageRepository = $entityManager->getRepository(Page::class);
            $pageId = $messageData['page_id'];

            /** @var Page $page */
            $page = $pageRepository->find($pageId);

            if ($page) {
                try {
                    $requiredConfigKeys = ['app_id', 'app_secret', 'default_graph_version', 'default_access_token'];

                    $defaultApp = Config::get('facebook.apps.default', $requiredConfigKeys);

                    $facebook = new Facebook($defaultApp);
                    $facebookPageReader = new PageReader($facebook, $entityManager, $output);
                    $pageProcessor = new PageProcessor($facebookPageReader, $entityManager);

                    $pageProcessor->execute($page);

                    $page->reject();
                    $entityManager->flush($page);

                    $output->success('Page sync success');
                    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                } catch (\Exception $e) {
                    $output->error($e->getMessage());
                    $message->delivery_info['channel']->basic_nack(
                        $message->delivery_info['delivery_tag'],
                        false,
                        true
                    );
                }
            } else {
                $output->error('failed - page (Id: ' . $pageId . ' not found.');
            }
        };

        return $callback;
    }
}