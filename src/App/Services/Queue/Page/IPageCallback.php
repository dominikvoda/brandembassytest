<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 24.07.17
 * Time: 22:51
 */

namespace App\Services\Queue\Page;

use App\Services\Output\IOutput;
use Doctrine\ORM\EntityManager;

interface IPageCallback
{
    /**
     * Return callback for page queue message
     *
     * @param EntityManager $entityManager
     * @param IOutput       $output
     *
     * @return callable
     */
    public static function get(EntityManager $entityManager, IOutput $output): callable;
}