<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 15.07.17
 * Time: 22:12
 */

namespace App\Facebook;

use App\Exceptions\UnexpectedResultException;
use Facebook\FacebookResponse;

class PostsResponse
{
    /**
     * @var string
     */
    protected $after;

    /**
     * @var array
     */
    protected $posts;

    /**
     * FacebookPostsResponse constructor.
     *
     * @param FacebookResponse $facebookResponse
     */
    public function __construct(FacebookResponse $facebookResponse)
    {
        $decodedBody = $facebookResponse->getDecodedBody();
        $this->posts = $decodedBody['data'];
        $this->after = $this->loadAfter($decodedBody);
    }

    /**
     * @param $decodedBody
     *
     * @return mixed
     */
    protected function loadAfter($decodedBody)
    {
        if (isset($decodedBody['paging']['cursors']['after'])) {
            return $decodedBody['paging']['cursors']['after'];
        }

        throw new UnexpectedResultException('Result missing cursor data');
    }

    /**
     * @return string
     */
    public function getAfter(): string
    {
        return $this->after;
    }

    /**
     * @return array
     */
    public function getPosts(): array
    {
        return $this->posts;
    }
}