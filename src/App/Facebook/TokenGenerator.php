<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 26.07.17
 * Time: 23:15
 */

namespace App\Facebook;

use App\Config;
use App\Exceptions\FacebookException;
use App\Utils\ConfigFile;
use App\Utils\Http;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

class TokenGenerator
{
    const API_ENDPOINT = '/oauth/access_token';

    /**
     * @var Facebook
     */
    private $facebook;

    /**
     * TokenGenerator constructor.
     */
    public function __construct(Facebook $facebook)
    {
        $this->facebook = $facebook;
    }

    /**
     * @param string $shortLivedToken
     *
     * @return string
     */
    public function generateToken(string $shortLivedToken): string
    {
        $endpoint = Http::createQueryString(
            self::API_ENDPOINT,
            [
                'grant_type'        => 'fb_exchange_token',
                'fb_exchange_token' => $shortLivedToken,
                'client_id'         => Config::get('facebook.apps.default.app_id'),
                'client_secret'     => Config::get('facebook.apps.default.app_secret'),
            ]
        );

        try {
            $response = $this->getFacebook()->get($endpoint);

            $result = $response->getDecodedBody();

            return $result['access_token'];
        } catch (FacebookSDKException $e) {
            throw new FacebookException($e->getMessage());
        }
    }

    protected function getFacebook(): Facebook
    {
        return $this->facebook;
    }
}