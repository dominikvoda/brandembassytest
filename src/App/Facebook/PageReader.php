<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 20:55
 */

namespace App\Facebook;

use App\Entities\Page;
use App\Entities\Post;
use App\Exceptions\InvalidFacebookPostException;
use App\Repositories\PostRepository;
use App\Services\Output\IOutput;
use App\Utils\Http;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Facebook\Facebook;

class PageReader
{
    /** Facebook API limit */
    const POSTS_LIMIT = 100;

    /** @var  Facebook */
    protected $facebook;

    /** @var EntityManager */
    protected $entityManager;

    /** @var  PostRepository */
    protected $postRepository;

    /** @var  IOutput */
    protected $output;

    /**
     * PageReader constructor.
     *
     * @param Facebook      $facebook
     * @param EntityManager $entityManager
     * @param IOutput       $output
     */
    public function __construct(Facebook $facebook, EntityManager $entityManager, IOutput $output)
    {
        $this->facebook = $facebook;
        $this->entityManager = $entityManager;
        $this->postRepository = $entityManager->getRepository(Post::class);
        $this->output = $output;
    }

    /**
     * Load page posts via Facebook API and return ArrayCollection of Post entities
     *
     * @param Page     $page
     * @param int|null $limit
     *
     * @return ArrayCollection
     */
    public function getPosts(Page $page, int $limit = null): ArrayCollection
    {
        if ($limit > self::POSTS_LIMIT) {
            $postsArray = $this->getPostsOverLimit($page, $limit);
        } else {
            $postResponse = $this->getPostsResponse($page, ['limit' => $limit]);
            $postsArray = $postResponse->getPosts();
        }

        return $this->parsePostsArray($postsArray);
    }


    /**
     * @param Page  $page
     * @param array $parameters
     *
     * @return PostsResponse
     */
    protected function getPostsResponse(Page $page, $parameters = []): PostsResponse
    {
        $requestParameters = $this->getRequestParameters($parameters);

        $endpoint = $this->getPostEndpoint(
            $page->getId(),
            $requestParameters
        );

        $response = $this->facebook->get($endpoint);

        return new PostsResponse($response);
    }

    /**
     * Get more than self::POSTS_LIMIT posts
     *
     * @param Page        $page
     * @param int         $limit
     * @param null|string $after
     * @param array       $posts
     *
     * @return array
     */
    protected function getPostsOverLimit(Page $page, int $limit, string $after = null, $posts = []): array
    {
        $postsLimit = $limit > self::POSTS_LIMIT ? self::POSTS_LIMIT : $limit;

        $requestParameters = $this->getRequestParameters(
            [
                'limit' => $postsLimit,
                'after' => $after,
            ]
        );

        $postResponse = $this->getPostsResponse($page, $requestParameters);

        $newPosts = $postResponse->getPosts();

        $posts = array_merge($posts, $newPosts);

        $newLimit = $limit - $postsLimit;

        if ($newLimit <= 0) {
            return $posts;
        }

        return $this->getPostsOverLimit($page, $newLimit, $postResponse->getAfter(), $posts);
    }

    /**
     * @param array $postsArray
     *
     * @return ArrayCollection
     */
    protected function parsePostsArray(array $postsArray): ArrayCollection
    {
        $existingPosts = $this->loadExistingPosts($postsArray);

        $posts = new ArrayCollection();
        foreach ($postsArray as $postItem) {
            try {
                if ($existingPosts->containsKey($postItem['id'])) {
                    /** @var Post $post */
                    $post = $existingPosts->get($postItem['id']);
                    $post->loadFromFacebookPost($postItem);
                } else {
                    $post = Post::createFromFacebookPost($postItem);
                }
                $posts->set($post->getId(), $post);
            } catch (InvalidFacebookPostException $e) {
                $this->output->error($e->getMessage());
            }
        }

        return $posts;
    }

    /**
     * Load existing posts
     *
     * @param array $postsArray
     *
     * @return ArrayCollection
     */
    protected function loadExistingPosts(array $postsArray): ArrayCollection
    {
        $ids = array_map(
            function ($postItem) {
                return $postItem['id'];
            },
            $postsArray
        );

        $posts = new ArrayCollection();

        /** @var Post $post */
        foreach ($this->postRepository->findPostByIds($ids) as $post) {
            $posts->set($post->getId(), $post);
        }

        return $posts;
    }

    /**
     * @param int   $pageId
     * @param array $parameters
     *
     * @return string
     */
    protected function getPostEndpoint(int $pageId, array $parameters = []): string
    {
        $endpoint = '/' . $pageId . '/posts';

        return Http::createQueryString($endpoint, $parameters);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    protected function getRequestParameters(array $parameters): array
    {
        $parameters['pretty'] = 0;

        return array_filter(
            $parameters,
            function ($parameter) {
                return null !== $parameter;
            }
        );
    }

}