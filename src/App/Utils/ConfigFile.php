<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 14:19
 */

namespace App\Utils;

/**
 * Class ConfigFile
 *
 * @deprecated
 */
class ConfigFile
{
    const APP = 'app.yml';
    const FACEBOOK = 'facebook.yml';
}