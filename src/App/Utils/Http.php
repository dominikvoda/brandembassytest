<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 26.07.17
 * Time: 23:20
 */

namespace App\Utils;

class Http
{
    /**
     * Build url with query string
     *
     * @param string $url
     * @param array  $parameters
     *
     * @return string
     */
    public static function createQueryString(string $url = '', $parameters = []): string
    {
        if (count($parameters) > 0) {
            $parameterString = http_build_query($parameters);
            $url .= '?' . $parameterString;
        }

        return $url;
    }
}