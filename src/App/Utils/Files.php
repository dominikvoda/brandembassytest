<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 26.07.17
 * Time: 22:35
 */

namespace App\Utils;

class Files
{
    /**
     * Create directory if not exists
     *
     * @param string $directory
     */
    public static function createDirectoryIfNotExist(string $directory)
    {
        if (null !== $directory && !is_dir($directory)) {
            $status = mkdir($directory, 0777, true);
            restore_error_handler();
            if (false === $status) {
                throw new \UnexpectedValueException(
                    sprintf('Directory not exists and cant\' be created ("%s")', $directory)
                );
            }
        }
    }

    /**
     * @param string $file
     *
     * @return bool
     */
    public static function fileExist(string $file): bool
    {
        return is_file($file);
    }
}