<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 13.07.17
 * Time: 15:25
 */

namespace App\Utils;

use Symfony\Component\Console\Output\OutputInterface;

class ProgressBar
{

    /**
     * @var string $format
     */
    protected $format;

    /**
     * @var \Symfony\Component\Console\Helper\ProgressBar $masterProgressBar
     */
    protected $masterProgressBar;

    /**
     * @var bool
     */
    protected $started;

    /**
     * @var OutputInterface
     */
    protected $output;


    /**
     * ProgressBar constructor.
     *
     * @param OutputInterface $output
     * @param int             $total
     * @param null|string     $title
     */
    public function __construct(OutputInterface $output, int $total = 0, string $title = null)
    {
        $this->masterProgressBar = new \Symfony\Component\Console\Helper\ProgressBar($output, $total);
        $this->format = ' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%';
        $this->output = $output;
        $this->masterProgressBar->setOverwrite(true);
        $this->started = false;

        if ($title) {
            $this->setTitle($title);
        }

        if ($total) {
            $this->start($total);
        }
    }

    /**
     * Set title before progress bar
     *
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->format = $title . $this->format;
    }


    /**
     * @param int $max
     *
     * @return bool
     */
    public function start(int $max = 0): bool
    {
        if ($this->started || $max == 0) {
            return false;
        }
        $this->started = true;
        $this->masterProgressBar->setRedrawFrequency($this->getRedrawFrequency($max));
        $this->masterProgressBar->setFormat($this->format);
        $this->masterProgressBar->start($max);

        return true;
    }

    /**
     * @param int $step
     */
    public function advance(int $step = 1)
    {
        if ($this->started) {
            $this->masterProgressBar->advance($step);
        } else {
            $this->output->writeln("ProgressBar not stated yet");
        }
    }

    /**
     *  Finish progress
     */
    public function finish()
    {
        if ($this->started) {
            $this->masterProgressBar->finish();
            $this->output->writeln('');
        }
    }

    /**
     * Update redraw frequency dependent on max count
     *
     * @param int $max
     *
     * @return int
     */
    protected function getRedrawFrequency(int $max): int
    {
        $frequency = 1;
        if ($max > 1000) {
            $frequency = 10;
        }
        if ($max > 10000) {
            $frequency = 100;
        }

        return $frequency;
    }

    /**
     * @return int
     */
    public function getProgress(): int
    {
        return $this->masterProgressBar->getProgress();
    }
}
