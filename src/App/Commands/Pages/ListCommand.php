<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 17:15
 */

namespace App\Commands\Pages;

use App\Commands\AbstractEntityManagerCommand;
use App\Entities\Page;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListCommand extends AbstractEntityManagerCommand
{
    protected function configure()
    {
        $this->setName('pages:list');
        $this->setDescription('List of all pages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pages = $this->loadPages();

        if (count($pages) === 0) {
            $this->io->note('Not found any pages');

            return;
        }

        $table = new Table($output);
        $table->setHeaders(['Id', 'Title', 'Last sync', 'In queue', 'Posts', 'Active', 'Period']);

        /** @var Page $page */
        foreach ($pages as $page) {
            $table->addRow(
                [
                    $page->getId(),
                    $page->getTitle(),
                    $page->getSyncedAt() ? $page->getSyncedAt()->format('Y-m-d H:i:s') : 'Not synced yet',
                    (int)$page->getInQueue(),
                    $page->getPosts()->count(),
                    $page->getActive() ? 'Active' : 'Inactive',
                    $page->getPeriod(),
                ]
            );
        }

        $table->render();
    }

    /**
     * @return array
     */
    protected function loadPages(): array
    {
        $pagesRepository = $this->getEntityManager()->getRepository(Page::class);

        return $pagesRepository->findAll();
    }

}