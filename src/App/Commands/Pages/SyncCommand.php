<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 20:47
 */

namespace App\Commands\Pages;

use App\Commands\AbstractEntityManagerCommand;
use App\Commands\QueueAccess;
use App\Entities\Page;
use App\Exceptions\QueueException;
use App\Repositories\PageRepository;
use App\Services\Queue\IMessageQueue;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SyncCommand extends AbstractEntityManagerCommand
{
    use QueueAccess;

    const PAGE_ARGUMENT = 'page';
    const FORCE_OPTION = 'force';
    const IGNORE_QUEUE_LOCKS = 'ignore-locks';
    const QUEUE_NAME = 'pages';

    protected function configure()
    {
        $this->setName('pages:sync');
        $this->setDescription('Push message to queue for every not synchronized page');
        $this->addArgument(self::PAGE_ARGUMENT, InputArgument::OPTIONAL, 'Page title');
        $this->addOption(self::FORCE_OPTION, 'f', InputOption::VALUE_NONE, 'Force sync all active pages');
        $this->addOption(self::IGNORE_QUEUE_LOCKS, null, InputOption::VALUE_NONE, 'Ignore queue locks');
    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle   $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pageTitle = $input->getArgument(self::PAGE_ARGUMENT);
        $pages = $this->getPages($pageTitle, $input->getOption(self::FORCE_OPTION));
        $ignoreLocks = $input->getOption(self::IGNORE_QUEUE_LOCKS);

        if (count($pages) === 0) {
            $this->getOutput()->writeLn('Nothing to sync');

            return;
        }

        /** @var IMessageQueue $queue */
        $queue = $this->getQueue('pages');

        /** @var Page $page */
        foreach ($pages as $page) {
            try {
                if ($this->resolvePage($queue, $page, $ignoreLocks)) {
                    $this->getOutput()->writeln(sprintf('Message published: [%s]', $page->getId()));
                }
            } catch (QueueException $e) {
                $this->getOutput()->error($e->getMessage());
            }
        }

        $queue->close();
    }

    protected function resolvePage(IMessageQueue $queue, Page $page, $ignoreLocks = false): bool
    {
        if ($page->inQueue() && !$ignoreLocks) {
            $this->getOutput()->warning('Page is already in queue');

            return false;
        }

        $message = [
            'page_id' => $page->getId(),
            'retry'   => 0,
        ];

        $queue->publishMessage(json_encode($message));

        $page->queued();
        $this->getEntityManager()->flush();

        return true;
    }


    /**
     * @param null|string $pageTitle
     * @param bool        $force
     *
     * @return array
     */
    protected function getPages(string $pageTitle = null, bool $force = false): array
    {
        /** @var PageRepository $pageRepository */
        $pageRepository = $this->getEntityManager()->getRepository(Page::class);

        if ($force) {
            return $pageRepository->findAllActive();
        }

        if (null !== $pageTitle) {
            $page = $pageRepository->findByTitle($pageTitle);

            return [$page];
        }

        return $pageRepository->findNotSyncedPages();
    }
}