<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 16:12
 */

namespace App\Commands\Pages;

use App\Commands\AbstractEntityManagerCommand;
use App\Entities\Page;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;

class AddCommand extends AbstractEntityManagerCommand
{
    protected function configure()
    {
        $this->setDescription('Step by step command which add new facebook page for synchronization');
        $this->setName('pages:add');
    }

    /**
     * @param InputInterface $input
     * @param SymfonyStyle   $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pageId = $this->getQuestionValue($input, $output, 'Facebook page ID: ');
        $title = $this->getQuestionValue($input, $output, 'Page title: ');
        $period = $this->getQuestionValue($input, $output, 'Download period [minutes]: ', 30);

        try {
            $this->createPage($pageId, $period, $title);
            $output->success('Page created');
        } catch (\Exception $e) {
            $output->error('Page not added: ' . $e->getMessage());
        }
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param string          $questionString
     * @param null|mixed      $defaultValue
     *
     * @return mixed
     */
    protected function getQuestionValue(
        InputInterface $input,
        OutputInterface $output,
        string $questionString,
        $defaultValue = null
    )
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $question = new Question($questionString, $defaultValue);

        return $helper->ask($input, $output, $question);
    }

    /**
     * @param $pageId
     * @param $interval
     * @param $title
     */
    protected function createPage(int $pageId, int $interval, string $title)
    {
        $page = new Page();
        $page->setId($pageId);
        $page->setPeriod($interval);
        $page->setActive(true);
        $page->setTitle($title);

        $this->getEntityManager()->persist($page);
        $this->getEntityManager()->flush($page);
    }
}