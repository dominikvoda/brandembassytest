<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 25.07.17
 * Time: 14:47
 */

namespace App\Commands;

use App\Config;
use App\Services\Output\IOutput;
use App\Services\Output\SymfonyConsoleOutput;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class AbstractOutputCommand extends Command
{
    /**
     * @var SymfonyStyle
     */
    protected $io;

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @return IOutput
     */
    protected function getOutput()
    {
        $logType = Config::get('sync.log');

        if ($logType === "console") {
            $log = new SymfonyConsoleOutput($this->io);
        } else {
            $logConfig = Config::get('logs.' . $logType . '.class');

            /** @var IOutput $log */
            $log = new $logConfig('sync_pages');
        }

        if (!$log instanceof IOutput) {
            throw new \InvalidArgumentException(
                sprintf('Class "%s" must implement interface "%s"', get_class($log), IOutput::class)
            );
        }

        return $log;
    }
}