<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 26.07.17
 * Time: 22:51
 */

namespace App\Commands\Facebook;

use App\Commands\AbstractOutputCommand;
use App\Config;
use App\Exceptions\FacebookException;
use App\Facebook\TokenGenerator;
use Facebook\Facebook;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class GenerateLongLiveTokenCommand extends AbstractOutputCommand
{
    const SHORT_LIVED_TOKEN_ARGUMENT = 'short-lived-token';

    const OVERWRITE_CONFIG = 'overwrite_config';

    protected function configure()
    {
        $this->setDescription('Generate long lived token from short lived token');
        $this->setName('facebook:generate-long-live-token');
        $this->addArgument(self::SHORT_LIVED_TOKEN_ARGUMENT, InputArgument::REQUIRED, 'Short lived token');
        $this->addOption(self::OVERWRITE_CONFIG, 'o', InputOption::VALUE_NONE, 'Overwrite config file with new token');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = new Facebook(Config::get('facebook.apps.default'));
        $tokenGenerator = new TokenGenerator($facebook);

        $shortLivedToken = $input->getArgument(self::SHORT_LIVED_TOKEN_ARGUMENT);

        try {
            $token = $tokenGenerator->generateToken($shortLivedToken);
            if ($input->getOption(self::OVERWRITE_CONFIG)) {
                $this->io->success(sprintf('New long lived token: %s', $token));
                $this->io->note('Config was not updated');
            } else {
                if ($this->updateConfigFile($token)) {
                    $this->io->success('Config file was updated');
                } else {
                    $this->io->error('Cant\' update config file');
                }
            }
        } catch (FacebookException $e) {
            $this->io->error($e->getMessage());
        }
    }

    /**
     * @param string $token
     *
     * @return bool
     */
    private function updateConfigFile(string $token): bool
    {
        $configData = Config::get();
        $configFile = Config::getConfigFile();

        $configData['facebook']['apps']['default']['default_access_token'] = $token;

        $newConfig = Yaml::dump($configData, 3);

        return false !== file_put_contents($configFile, $newConfig);
    }

}