<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 14:32
 */

namespace App\Commands;

use Facebook\Facebook;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Sandbox extends Command
{
    protected function configure()
    {
        $this->setName('sandbox');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $facebook = new Facebook(
            [
                'app_id'                => '1276149575845851',
                'app_secret'            => 'f99816676ef7462e4d873c43b2b1efd9',
                'default_graph_version' => 'v2.9',
                'default_access_token'  => 'EAASIpromB9sBAGN9gqPd5eSpzjAZAECE64J51hkhbUN6ibvN0A3vtWRYEqZAPMBW6d3GQC76wxzIzTfSdfZAAnOJHeKL3XIjUOaahq4pujQ6RobuQoFZAJJrHSZCxhZBUFezZC98E6jTO0GnYDWaZBjd7oZCApGZA6reCNvAdVZBXb3kARMb2M9hD3G5rJkZA5qzc8AZD',
            ]
        );

        try {
            // Get the \Facebook\GraphNodes\GraphUser object for the current user.
            // If you provided a 'default_access_token', the '{access-token}' is optional.
            $response = $facebook->get('/120461304659141/posts');
            var_dump($response->getBody());
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }
}