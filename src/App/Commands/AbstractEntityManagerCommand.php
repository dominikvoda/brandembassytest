<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 16:13
 */

namespace App\Commands;

use Doctrine\ORM\EntityManager;

abstract class AbstractEntityManagerCommand extends AbstractOutputCommand
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * AbstractEntityManagerCommand constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct(null);
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->entityManager;
    }
}