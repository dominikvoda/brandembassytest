<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 18.07.17
 * Time: 16:25
 */

namespace App\Commands\Queue;

use App\Commands\AbstractEntityManagerCommand;
use App\Commands\QueueAccess;
use App\Services\Queue\IMessageQueue;
use App\Services\Queue\Page\IPageCallback;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PageConsumerStartCommand extends AbstractEntityManagerCommand
{
    use QueueAccess;

    const QUEUE_NAME = 'pages';

    protected function configure()
    {
        $this->setDescription('Start page consumer depend on config');
        $this->setName('queue:page-consumer-start');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var IMessageQueue $queue */
        $queue = $this->getQueue(self::QUEUE_NAME);
        $entityManager = $this->getEntityManager();

        /** @var IPageCallback $callbackClass */
        $callbackClass = $this->getQueueCallback(self::QUEUE_NAME);
        $callback = $callbackClass::get($entityManager, $this->getOutput());

        $this->getOutput()->success('Page receiver start');
        $queue->consumeMessages($callback, $this->getOutput());
    }
}