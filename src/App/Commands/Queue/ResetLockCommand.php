<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 24.07.17
 * Time: 23:19
 */

namespace App\Commands\Queue;

use App\Commands\AbstractEntityManagerCommand;
use App\Entities\Page;
use App\Repositories\PageRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ResetLockCommand extends AbstractEntityManagerCommand
{
    protected function configure()
    {
        $this->setDescription('Set all pages in_queue = false');
        $this->setName('queue:reset-locks');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var PageRepository $pageRepository */
        $pageRepository = $this->getEntityManager()->getRepository(Page::class);
        $pageRepository->resetQueueLocks();

        $this->io->success('All pages are unlocked');
    }

}