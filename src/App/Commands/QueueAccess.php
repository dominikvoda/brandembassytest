<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 29.07.17
 * Time: 23:44
 */

namespace App\Commands;

use App\Config;
use App\Services\Queue\IMessageQueue;

trait QueueAccess
{
    /**
     * @param $queueName
     *
     * @return IMessageQueue
     */
    protected function getQueue($queueName): IMessageQueue
    {
        $queueClass = $this->getQueueClass($queueName);

        /** @var IMessageQueue $queue */
        $queue = new $queueClass('pages');

        if ($queue instanceof IMessageQueue) {
            return $queue;
        }

        throw new \InvalidArgumentException(
            sprintf('Class "%s" must implement interface "%s"', get_class($queue), IMessageQueue::class)
        );
    }

    /**
     * @return string
     */
    public function getQueueCallback($queueName): string
    {
        return Config::get('queues.' . $this->getQueueType($queueName) . '.consumers.' . $queueName . '.callback');
    }

    /**
     * @param $queueName
     *
     * @return string
     */
    private function getQueueType($queueName): string
    {
        return Config::get('sync.queues.' . $queueName);
    }

    /**
     * @return string
     */
    private function getQueueClass($queueName): string
    {
        return Config::get('queues.' . $this->getQueueType($queueName) . '.class');
    }
}