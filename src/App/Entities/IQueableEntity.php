<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 29.07.17
 * Time: 23:56
 */

namespace App\Entities;

interface IQueableEntity
{
    /**
     * @param int $period
     *
     * @return IQueableEntity
     */
    public function setPeriod(int $period): IQueableEntity;

    /**
     * @return int
     */
    public function getPeriod(): int;

    /**
     * @param \DateTime $syncedAt
     *
     * @return IQueableEntity
     */
    public function setSyncedAt(\DateTime $syncedAt): IQueableEntity;

    /**
     * @return \DateTime
     */
    public function getSyncedAt(): \DateTime;

    /**
     * @param bool $isActive
     *
     * @return IQueableEntity
     */
    public function setActive(bool $isActive): IQueableEntity;

    /**
     * @return bool
     */
    public function getActive(): bool;

    /**
     * @param bool $inQueue
     *
     * @return IQueableEntity
     */
    public function setInQueue(bool $inQueue): IQueableEntity;

    /**
     * @return bool
     */
    public function getInQueue(): bool;

    /**
     * @param \DateTime|null $date
     *
     * @return void
     */
    public function synced(\DateTime $date = null);

    /**
     * @return void
     */
    public function queued();

    /**
     * @return void
     */
    public function reject();
}