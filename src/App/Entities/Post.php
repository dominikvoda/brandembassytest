<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 12.07.17
 * Time: 11:20
 */

namespace App\Entities;

use App\Exceptions\InvalidFacebookPostException;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Page
 * @ORM\Entity(repositoryClass="App\Repositories\PostRepository")
 */
class Post
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=64)
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $message;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $story;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entities\Page", inversedBy="posts")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id")
     */
    protected $page;

    /**
     * Create entity from facebook post data array
     *
     * @param $postArray
     *
     * @return Post
     */
    public static function createFromFacebookPost($postArray)
    {
        $post = new Post();
        $post->setId($postArray['id']);
        $post->loadFromFacebookPost($postArray);

        return $post;
    }

    /**
     * Update entity from facebook post data array
     *
     * @param $postArray
     */
    public function loadFromFacebookPost($postArray)
    {
        if (!isset($postArray['message']) || null === $postArray['message']) {
            throw new InvalidFacebookPostException('Missing post message', $postArray);
        }

        $createdAt = new \DateTime($postArray['created_time']);
        $this->setCreatedAt($createdAt);
        $this->setMessage($postArray['message']);

        if (isset($postArray['story'])) {
            $this->setStory($postArray['story']);
        }
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Post
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Post
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set page
     *
     * @param \App\Entities\Page $page
     *
     * @return Post
     */
    public function setPage(\App\Entities\Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return \App\Entities\Page
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set story
     *
     * @param string $story
     *
     * @return Post
     */
    public function setStory($story)
    {
        $this->story = $story;

        return $this;
    }

    /**
     * Get story
     *
     * @return string
     */
    public function getStory()
    {
        return $this->story;
    }
}
