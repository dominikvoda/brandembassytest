<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 29.07.17
 * Time: 23:54
 */

namespace App\Entities\Traits;

use App\Entities\IQueableEntity;

trait QueableEntity
{
    /**
     * @ORM\Column(type="integer")
     */
    protected $period;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $syncedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $inQueue = true;


    /**
     * Set period
     *
     * @param integer $period
     *
     * @return IQueableEntity|QueableEntity
     */
    public function setPeriod(int $period): IQueableEntity
    {
        $this->period = $period;

        return $this;
    }

    /**
     * Get period
     *
     * @return integer
     */
    public function getPeriod(): int
    {
        return $this->period;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return IQueableEntity|QueableEntity
     */
    public function setSyncedAt(\DateTime $syncedAt): IQueableEntity
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt(): \DateTime
    {
        return $this->syncedAt;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return IQueableEntity|QueableEntity
     */
    public function setActive(bool $active): IQueableEntity
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * Set inQueue
     *
     * @param boolean $inQueue
     *
     * @return IQueableEntity|QueableEntity
     */
    public function setInQueue(bool $inQueue): IQueableEntity
    {
        $this->inQueue = $inQueue;

        return $this;
    }

    /**
     * Get inQueue
     *
     * @return boolean
     */
    public function getInQueue(): bool
    {
        return $this->inQueue;
    }

    /**
     * @return bool
     */
    public function inQueue(): bool
    {
        return $this->getInQueue();
    }

    /**
     * Set synced at time
     *
     * @param \DateTime|null $date
     */
    public function synced(\DateTime $date = null)
    {
        $syncDate = $date ?: new \DateTime();
        $this->setSyncedAt($syncDate);
    }

    /**
     * Set entity in queue status
     */
    public function queued()
    {
        $this->setInQueue(true);
    }

    /**
     * Set entity not in queue status
     */
    public function reject()
    {
        $this->setInQueue(false);
    }
}