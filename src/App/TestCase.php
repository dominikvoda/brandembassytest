<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 30.07.17
 * Time: 1:11
 */

namespace App;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use DoctrineExtensions\Query\Mysql\Now;
use DoctrineExtensions\Query\Mysql\TimestampDiff;

class TestCase extends \PHPUnit\Framework\TestCase
{
    private $entityManager;

    protected function setUp()
    {
        parent::setUp();
        $this->entityManager = $this->loadEntityManager();
    }

    public function getEntityManager(): EntityManager
    {
        return $this->entityManager;
    }

    private function loadEntityManager()
    {
        $configFile = __DIR__ . '/../../src/config/app.test.yml';
        Config::loadConfigFile($configFile);

        $isDevMode = true;
        $ormConfig = Setup::createAnnotationMetadataConfiguration([__DIR__ . "/src"], $isDevMode, null, null, false);

        $ormConfig->setNamingStrategy(new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy());
        $ormConfig->addCustomDatetimeFunction('timestampdiff', TimestampDiff::class);
        $ormConfig->addCustomDatetimeFunction('now', Now::class);

        $connection = Config::get('database');

        // obtaining the entity manager
        $entityManager = EntityManager::create($connection, $ormConfig);

        return $entityManager;
    }

}