<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 14:00
 */

require_once "bootstrap.php";

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);