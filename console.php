#!/usr/bin/env php
<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 13:51
 */

require_once __DIR__ . '/bootstrap.php';

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new \App\Commands\Sandbox());
$application->add(new \App\Commands\Facebook\GenerateLongLiveTokenCommand());

$application->add(new \App\Commands\Pages\AddCommand($entityManager));
$application->add(new \App\Commands\Pages\ListCommand($entityManager));
$application->add(new \App\Commands\Pages\SyncCommand($entityManager));

$application->add(new \App\Commands\Queue\PageConsumerStartCommand($entityManager));
$application->add(new \App\Commands\Queue\ResetLockCommand($entityManager));

$application->run();
