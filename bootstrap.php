<?php
/**
 * Created by PhpStorm.
 * User: Dominik Voda
 * Date: 11.07.17
 * Time: 13:58
 */

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use App\Config;

require_once "vendor/autoload.php";

$isDevMode = true;
$ormConfig = Setup::createAnnotationMetadataConfiguration([__DIR__ . "/src"], $isDevMode, null, null, false);

$ormConfig->setNamingStrategy(new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy());
$ormConfig->addCustomDatetimeFunction('timestampdiff', DoctrineExtensions\Query\Mysql\TimestampDiff::class);
$ormConfig->addCustomDatetimeFunction('now', DoctrineExtensions\Query\Mysql\Now::class);

$configFile = __DIR__ . '/src/config/config.yml';
Config::loadConfigFile($configFile);

$connection = Config::get('database');

// obtaining the entity manager
$entityManager = EntityManager::create($connection, $ormConfig);


