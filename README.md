# BrandEmbassy Test #

### Flow ###
Every 10 seconds, it checks if there is a page that has not been synchronized for longer than its set period. For this page, a message is added to the queue and the page is locked until it is processed by the consumer.

The consumer reads page id from message. Then via Facebook Graph API download posts data. This data compares with the database and update/create it.

### Technologies ###
* PHP 7.0.15+
* MySql
* Supervisor
* RabbitMQ

### Packages ###
* Doctrine/ORM
* Symfony/Console
* Facebook/Graph-SDK
* Doctrine/Migrations
* PHP-Amqplib
* Beberlei/DoctrineExtensions - sql date functions support
* Monolog


### Set up ###

* Create MySql database
* Run `composer install`
* Create config file from `src/App/config/config.yml`
* Set database connection (RabbitMQ connection if needed)
* Update config file path in `bootstrap.php`
* Create database schema `php console.php bin/vendor/doctrine doctrine:schema-tool:create`

### Run ###
Just run those two shell scripts 

* `sh supervisor-page-consumer.sh` - every 10 seconds check for not sync pages
* `sh supervisor-cron.sh` - start queue consumer

(supervisor recommended [supervisor.cron.dist])

### Console commands ###

Commands description via `php console.php list`

### Tests ###

Run `php vendor/bin/phpcs`
